package mobi.trucky.tacos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLiteHelper extends SQLiteOpenHelper {
	public static final String TABLE_TRUCKS = "trucks";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_TRUCK = "truck";
	public static final String TABLE_STREETS = "streets";
	public static final String COLUMN_STREET = "street";
	public static final String COLUMN_ZIP = "zip";
	public static final String COLUMN_TWITTER = "twitter";
	public static final String TABLE_TIMES = "times";
	public static final String COLUMN_DAY = "day";
	public static final String COLUMN_OPEN = "open";
	public static final String TABLE_EVENTS = "events";
	public static final String COLUMN_TIMEID = "timeid";
	public static final String COLUMN_STREETID = "streetid";
	public static final String COLUMN_TRUCKID = "truckid";
	public static final String VIEW_EVENTS = "eventsview";
	
	private static final String DATABASE_NAME = "trucks.db";
	private static final int DATABASE_VERSION = 6;

	private static final String TRUCKS_CREATE = "create table "
	  + TABLE_TRUCKS + "(" + COLUMN_ID
	  + " integer primary key autoincrement, " + COLUMN_TRUCK
	  + " text not null);";
	private static final String STREETS_CREATE = "create table "
	  + TABLE_STREETS + "(" + COLUMN_ID
	  + " integer primary key autoincrement, " + COLUMN_STREET
	  + " text not null, " + COLUMN_ZIP + " text not null, " + COLUMN_TWITTER + " text not null);";
	private static final String TIMES_CREATE = "create table "
	  + TABLE_TIMES + "(" + COLUMN_ID
	  + " integer primary key autoincrement, " + COLUMN_DAY
	  + " text not null, " + COLUMN_OPEN + " text not null);";
	private static final String EVENTS_CREATE = "create table "
	  + TABLE_EVENTS + "(" + COLUMN_ID
	  + " integer primary key autoincrement, " + COLUMN_TIMEID
	  + " integer , " + COLUMN_STREETID + " integer not null, " + COLUMN_TRUCKID + " integer not null);";

	private static final String EVENTSVIEW_CREATE = "create view "+VIEW_EVENTS+" as select "
			+ " AA._ID " +COLUMN_TRUCKID+ ", AA." +COLUMN_TRUCK
			+ " , BB._ID " +COLUMN_STREETID+ ", BB." +COLUMN_STREET + ", BB." +COLUMN_ZIP + ", BB." +COLUMN_TWITTER
			+ " FROM " +TABLE_EVENTS+" ZZ JOIN "+TABLE_TRUCKS+" AA ON ZZ." +COLUMN_TRUCKID+ " =AA._ID JOIN "+TABLE_STREETS+" BB ON ZZ." +COLUMN_STREETID+ " =BB._ID"
			;

	public MySQLiteHelper(Context context) {
	  super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(TRUCKS_CREATE);
		db.execSQL(STREETS_CREATE);
		db.execSQL(TIMES_CREATE);
		db.execSQL(EVENTS_CREATE);
		db.execSQL(EVENTSVIEW_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRUCKS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_STREETS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TIMES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);
		db.execSQL("DROP VIEW IF EXISTS " + VIEW_EVENTS);
	    onCreate(db);
	}

}
