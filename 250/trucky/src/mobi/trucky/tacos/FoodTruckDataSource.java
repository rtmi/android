package mobi.trucky.tacos;

import mobi.trucky.tacos.R;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class FoodTruckDataSource {

	  private SQLiteDatabase database;
	  private MySQLiteHelper dbHelper;
	  private String[] allColumns = { MySQLiteHelper.COLUMN_TRUCKID,
	      MySQLiteHelper.COLUMN_TRUCK, MySQLiteHelper.COLUMN_STREETID,
	      MySQLiteHelper.COLUMN_STREET, MySQLiteHelper.COLUMN_ZIP, MySQLiteHelper.COLUMN_TWITTER};

	  public FoodTruckDataSource(Context context){
		dbHelper = new MySQLiteHelper(context);
	  }

	  public void open() throws SQLException {
	    database = dbHelper.getWritableDatabase();
	  }

	  public void close() {
	    dbHelper.close();
	  }

	  //todo Not always insert, fetch existing truck/street
	  public FoodTruckModel createModel(String truck, String street, String zip, String twitter) {
	    ContentValues values = new ContentValues();

	    values.put(MySQLiteHelper.COLUMN_TRUCK, truck);
	    long truckId = database.insert(MySQLiteHelper.TABLE_TRUCKS, null,
	        values);
	    values.clear();

	    values.put(MySQLiteHelper.COLUMN_STREET, street);
	    values.put(MySQLiteHelper.COLUMN_ZIP, zip);
	    values.put(MySQLiteHelper.COLUMN_TWITTER, twitter);
	    long streetId = database.insert(MySQLiteHelper.TABLE_STREETS, null,
		        values);
	    values.clear();

	    values.put(MySQLiteHelper.COLUMN_TRUCKID, truckId);
	    values.put(MySQLiteHelper.COLUMN_STREETID, streetId);
	    long eventId = database.insert(MySQLiteHelper.TABLE_EVENTS, null,
		        values);
	    values.clear();
	    
	    Cursor cursor = database.query(MySQLiteHelper.VIEW_EVENTS,
	        allColumns, MySQLiteHelper.COLUMN_TRUCKID + " = " + truckId, null,
	        null, null, null);
	    cursor.moveToFirst();
	    FoodTruckModel newModel = cursorToModel(cursor);
	    cursor.close();
	    return newModel;
	  }
	  public FoodTruckModel createWeeklyEvent(FoodTruckModel truck, List<EventTimeModel> schedule){
		  //TODO
		  return truck;
	  }

	public List<FoodTruckModel> getAll() {
		return getTrucksByQuery(null);
	}

	public List<FoodTruckModel> getTrucksByZip(String zip) {
		return getTrucksByQuery(MySQLiteHelper.COLUMN_ZIP + " ='" + zip + "'");
	}

	private List<FoodTruckModel> getTrucksByQuery(String whereClause){
		List<FoodTruckModel> items = new ArrayList<FoodTruckModel>();
	
		Cursor cursor = database.query(MySQLiteHelper.VIEW_EVENTS, 
			allColumns, whereClause, null,
			null, null, null);
	
		cursor.moveToFirst();
	
		while (!cursor.isAfterLast()) {
			FoodTruckModel i = cursorToModel(cursor);
			items.add(i);
			cursor.moveToNext();
		}
	
		cursor.close();
		return items;
	}

	  private FoodTruckModel cursorToModel(Cursor cursor) {
		FoodTruckModel i = new FoodTruckModel();
		i.setTruckId(cursor.getLong(0));
		i.setTruck(cursor.getString(1));
		i.setStreetId(cursor.getLong(2));
		i.setStreet(cursor.getString(3));
		i.setZip(cursor.getString(4));
		i.setTwitter(cursor.getString(5));
		return i;
	  }

}
