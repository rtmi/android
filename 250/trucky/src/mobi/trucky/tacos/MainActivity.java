package mobi.trucky.tacos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import mobi.trucky.tacos.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MainActivity extends android.support.v4.app.FragmentActivity
	implements EditGeoDialogFragment.EditGeoDialogListener, 
			SearchTwitterDialogFragment.SearchTwitterDialogListener,
			AddTruckDialogFragment.AddTruckDialogListener,
			AddWeekdayDialogFragment.AddWeekdayDialogListener, EditZipDialogFragment.EditZipDialogListener {

	private FoodTruckDataSource datasource;
	private FoodTruckModel foodtruck;
	private SharedPreferences pref;
    /**
     * Note that this may be null if the Google Play services APK is not available.
     */
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpMapIfNeeded();
        android.app.ActionBar actionBar = getActionBar();
        actionBar.setDisplayOptions(android.app.ActionBar.DISPLAY_SHOW_HOME
            | android.app.ActionBar.DISPLAY_SHOW_TITLE | android.app.ActionBar.DISPLAY_SHOW_CUSTOM);


		pref = this.getSharedPreferences("mobi.trucky.tacos", 0);
		
		datasource = new FoodTruckDataSource(this);
		datasource.open();
    }

    @Override
    protected void onResume() {
        datasource.open();
        super.onResume();
        setUpMapIfNeeded();
    }
    @Override
    protected void onPause() {
      datasource.close();
      super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
      switch (item.getItemId()) {
      case R.id.action_settings:
    	  new EditZipDialogFragment().show(getSupportFragmentManager(), "preferences");
        break;
      case R.id.action_twitter:
    	  new SearchTwitterDialogFragment().show(getSupportFragmentManager(), "searchtwitter");
    	  break;
      case R.id.action_add:
    	  //new EditGeoDialogFragment().show(getSupportFragmentManager(), "editgeo");
    	  new AddTruckDialogFragment().show(getSupportFragmentManager(), "addtruck");
          break;

      default:
        break;
      }

      return true;
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView
     * MapView}) will show a prompt for the user to install/update the Google Play services APK on
     * their device.
     * <p>
     * A user can return to this Activity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the Activity may not have been
     * completely destroyed during this process (it is likely that it would only be stopped or
     * paused), {@link #onCreate(Bundle)} may not be called again so we should call this method in
     * {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker.
     * <p>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        //mMap.addMarker(new MarkerOptions().position(new LatLng(47.60625690, -122.33297310)).title("Marker"));
    }

	@Override
	public void onEditGeoDialogPositiveClick(String zip) {
	    ConnectivityManager connMgr = (ConnectivityManager) 
	            getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            // fetch data            
    		new GeotestTask().execute(zip);
        } else {
            // display error
            Toast.makeText(this, "Network required", Toast.LENGTH_SHORT)
            .show();
        }
        
	}

	@Override
	public void onEditGeoDialogNegativeClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}
	
	private class GeotestTask extends AsyncTask<String, Void, Geolocation>{
	    private static final String GEOCODE_PAGE =
	            "http://maps.googleapis.com/maps/api/geocode/json?address=%s&" +
	            "sensor=false";
	    
		@Override
		protected Geolocation doInBackground(String... params) {
			Geolocation result = null;
			String url = String.format(GEOCODE_PAGE, Uri.encode(params[0]));
			String json = GeocodeHelper.getUrlContent(url);
			result = GeocodeHelper.getPageContent(json);
			result.name = params[1];
			return result;
		}
		
		@Override
	    protected void onPostExecute(Geolocation result) {
			Toast.makeText(MainActivity.this, "location: " + result.Lat + ","+ result.Lng, Toast.LENGTH_LONG)
			.show();
	        mMap.addMarker(new MarkerOptions().position(new LatLng(result.Lat, result.Lng)).title(result.name));
	        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(result.Lat, result.Lng), 14));
		}


	}

	@Override
	public void onSearchTwitterDialogPositiveClick(String name) {
	    ConnectivityManager connMgr = (ConnectivityManager) 
	            getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            // fetch data            
    		new TwittertestTask().execute(name);
        } else {
            // display error
            Toast.makeText(this, "Network required", Toast.LENGTH_SHORT)
            .show();
        }
	}

	@Override
	public void onSearchTwitterDialogNegativeClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}
	
	private class TwittertestTask extends AsyncTask<String, Void, List<twitter4j.Status>>{

		@Override
		protected List<twitter4j.Status> doInBackground(String... params) {
			return TwitterHelper.getPageContent(params[0]);
		}

		@Override
	    protected void onPostExecute(List<twitter4j.Status> tweets) {
			String sentence ="";
			int count = 0;
            for (twitter4j.Status tweet : tweets) {
                Log.i(MainActivity.class.toString(), "@" + tweet.getUser().getScreenName() + " - " + tweet.getText());
                sentence =sentence +"\n" +"@" + tweet.getUser().getScreenName() + " - " + tweet.getText();
                count = count +1;
                if (count >4){
                	break;
                }
            }
			//
            Toast.makeText(MainActivity.this, sentence, Toast.LENGTH_SHORT)
            .show();
		}

	}

	public void onDialogPositiveClick(String truck, String street, String zip, String twitter) {
        foodtruck = datasource.createModel(truck, street, zip, twitter);

        new AddWeekdayDialogFragment().show(getSupportFragmentManager(), "addweekdays");

//        ArrayAdapter<FoodTruckModel> adapter = (ArrayAdapter<FoodTruckModel>) getListAdapter();
//        adapter.add(foodtruck);
//        adapter.notifyDataSetChanged();

        new GeotestTask().execute(street +", " +zip, twitter);
	}

	public void onDialogNegativeClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}

	public void onAddWeekdayDialogPositiveClick(List<EventTimeModel> schedule) {
		datasource.createWeeklyEvent(foodtruck, schedule);
	}

	public void onAddWeekdayDialogNegativeClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}

	public void onEditZipDialogPositiveClick(String zip) {
		Editor ed = pref.edit();
		ed.putString("mobi.trucky.tacos", zip);
		ed.commit();
//		DisplayTruckList();
	}

	public void onEditZipDialogNegativeClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}
}