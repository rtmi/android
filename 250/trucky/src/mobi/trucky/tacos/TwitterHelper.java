package mobi.trucky.tacos;

import java.util.List;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterHelper {
    private static final String TAG = "TwitterHelper";

    public static List<twitter4j.Status> getPageContent(String name)
    {
    	ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
		  .setOAuthConsumerKey("#")
		  .setOAuthConsumerSecret("#")
		  .setOAuthAccessToken("#")
		  .setOAuthAccessTokenSecret("#");
		TwitterFactory tf = new TwitterFactory(cb.build());
		Twitter twitter = tf.getInstance();
		try {
          Query query = new Query(name);
          QueryResult result;
          result = twitter.search(query);
          return result.getTweets();
          
	      } catch (TwitterException te) {
	    	  te.printStackTrace();
	      }
		
		return null;

    }
}
