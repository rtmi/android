package mobi.trucky.tacos;

import java.util.ArrayList;
import java.util.List;

import mobi.trucky.tacos.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class AddWeekdayDialogFragment extends DialogFragment {
	public interface AddWeekdayDialogListener {
        public void onAddWeekdayDialogPositiveClick(List<EventTimeModel> schedule);
        public void onAddWeekdayDialogNegativeClick(DialogFragment dialog);
    }
    
    // Use this instance of the interface to deliver action events
	AddWeekdayDialogListener mListener;
	
	private ArrayList<Integer> mSelectedItems;

	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the DialogListener so we can send events to the host
            mListener = (AddWeekdayDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement AddWeekdayDialogListener");
        }
    }

	@Override
	public Dialog onCreateDialog(Bundle SavedInstanceState){
		mSelectedItems = new ArrayList<Integer>();
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.addweekday_title)
		.setMultiChoiceItems(R.array.weekdays, null,
                new DialogInterface.OnMultiChoiceClickListener() {
         public void onClick(DialogInterface dialog, int which, boolean isChecked) {
             if (isChecked) {
                 // If the user checked the item, add it to the selected items
                 mSelectedItems.add(which);
             } else if (mSelectedItems.contains(which)) {
                 // Else, if the item is already in the array, remove it 
                 mSelectedItems.remove(Integer.valueOf(which));
             }
         }
     })
     .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
         public void onClick(DialogInterface dialog, int id) {
        	 List<EventTimeModel> schedule = new ArrayList<EventTimeModel>();
        	 for (int i=0; i< mSelectedItems.size();i++){
        		 int which = mSelectedItems.get(i);
        		 EventTimeModel ev = new EventTimeModel();
        		 ev.setDayOfMonth(which);
        		 ev.setMonthOfYear(0);
        		 ev.setYear(0);
        		 schedule.add(ev);
        	 }
        	 mListener.onAddWeekdayDialogPositiveClick(schedule);
         }
     })
     .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
         public void onClick(DialogInterface dialog, int id) {
         }
     });

		return builder.create();
    }
}
