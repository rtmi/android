package mobi.trucky.tacos;

import mobi.trucky.tacos.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class AddTruckDialogFragment extends DialogFragment {
	public interface AddTruckDialogListener {
        public void onDialogPositiveClick(String truck, String street, String zip, String twitter);
        public void onDialogNegativeClick(DialogFragment dialog);
    }
    
    // Use this instance of the interface to deliver action events
	AddTruckDialogListener mListener;
	
	private EditText mEditTextAddtruck;
	private EditText mEditTextAddstreet;
	private EditText mEditTextAddzip;
	private EditText mEditTextAddtwitter;
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the DialogListener so we can send events to the host
            mListener = (AddTruckDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement AddTruckDialogListener");
        }
    }

    @Override
	public Dialog onCreateDialog(Bundle SavedInstanceState){
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view =inflater.inflate(R.layout.dialog_addtruck, null);
		mEditTextAddtruck = (EditText) view.findViewById(R.id.addtruck);
		mEditTextAddstreet = (EditText) view.findViewById(R.id.addstreet);
		mEditTextAddzip = (EditText) view.findViewById(R.id.addzip);
		mEditTextAddtwitter = (EditText) view.findViewById(R.id.addtwitter);

	    builder.setView(view)
	           .setPositiveButton(R.string.next, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	            	// Send the positive button event back to the host activity
                       mListener.onDialogPositiveClick(mEditTextAddtruck.getText().toString(), mEditTextAddstreet.getText().toString(), mEditTextAddzip.getText().toString(), mEditTextAddtwitter.getText().toString());
	               }
	           })
	           .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	            	// Send the negative button event back to the host activity
                       mListener.onDialogNegativeClick(AddTruckDialogFragment.this);
	               }
	           });
		return builder.create();
	}
}
