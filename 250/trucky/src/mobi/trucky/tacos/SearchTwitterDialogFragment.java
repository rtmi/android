package mobi.trucky.tacos;

import mobi.trucky.tacos.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class SearchTwitterDialogFragment extends DialogFragment {

	public interface SearchTwitterDialogListener{
		public void onSearchTwitterDialogPositiveClick(String name);
		public void onSearchTwitterDialogNegativeClick(DialogFragment dialog);
	}
	
	SearchTwitterDialogListener mListener;
	
	private EditText mEditTextSearchtwitter;

	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		try{
			mListener = (SearchTwitterDialogListener)activity;
		}catch (ClassCastException e){
			throw new ClassCastException(activity.toString()
					+ "must implement SearchTwitterDialogListener");
		}
	}
	
	@Override
	public Dialog onCreateDialog(Bundle SavedInstanceState){
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.dialog_twitter, null);
		mEditTextSearchtwitter = (EditText)view.findViewById(R.id.searchtwitter);
		
		builder.setView(view).setTitle(R.string.searchtwitter_title)
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					mListener.onSearchTwitterDialogPositiveClick(mEditTextSearchtwitter.getText().toString());
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					mListener.onSearchTwitterDialogNegativeClick(SearchTwitterDialogFragment.this);
				}
			});
	
		return builder.create();
	}
}
