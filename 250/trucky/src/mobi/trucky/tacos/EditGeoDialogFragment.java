package mobi.trucky.tacos;

import mobi.trucky.tacos.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;


public class EditGeoDialogFragment extends DialogFragment {



	public interface EditGeoDialogListener{
        public void onEditGeoDialogPositiveClick(String zip);
        public void onEditGeoDialogNegativeClick(DialogFragment dialog);
    }
    
	EditGeoDialogListener mListener;

	private EditText mEditTextEditgeo;

	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
        	mListener = (EditGeoDialogListener) activity;
        }catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement EditGeoDialogListener");
        }
    }

    @Override
	public Dialog onCreateDialog(Bundle SavedInstanceState){
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view =inflater.inflate(R.layout.dialog_geo, null);
		mEditTextEditgeo = (EditText) view.findViewById(R.id.editgeo);

		builder.setView(view).setTitle(R.string.editgeo_title)
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					mListener.onEditGeoDialogPositiveClick(mEditTextEditgeo.getText().toString());
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					mListener.onEditGeoDialogNegativeClick(EditGeoDialogFragment.this);
				}
			});
	
		return builder.create();
	}
}
