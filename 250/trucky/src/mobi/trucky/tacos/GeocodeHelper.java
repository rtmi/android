package mobi.trucky.tacos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class GeocodeHelper {
    private static final String TAG = "GeocodeHelper";

    public static Geolocation getPageContent(String json)
	{
		try {
			// Drill into the JSON response to find the content body
	        JSONObject response = new JSONObject(json);
	        JSONArray results = response.getJSONArray("results");
	        JSONObject result= results.getJSONObject(0);
	        JSONObject geometry = result.getJSONObject("geometry");
	        JSONObject location = geometry.getJSONObject("location");
	        double lat = location.getDouble("lat");
	        double lng = location.getDouble("lng");

	        Geolocation loc = new Geolocation();
	        loc.Lat = lat;
	        loc.Lng = lng;
	        return loc;
	    } catch (JSONException e) {
	        //throw new ParseException("Problem parsing API response", e);
	    	e.printStackTrace();
	    }
		
		return null;
	}
	
	public static String getUrlContent(String url)
	{
		StringBuilder builder = new StringBuilder();
	    HttpClient client = new DefaultHttpClient();
	    HttpGet httpGet = new HttpGet(url);
	    try {
	      HttpResponse response = client.execute(httpGet);
	      StatusLine statusLine = response.getStatusLine();
	      int statusCode = statusLine.getStatusCode();
	      if (statusCode == 200) {
	        HttpEntity entity = response.getEntity();
	        InputStream content = entity.getContent();
	        BufferedReader reader = new BufferedReader(new InputStreamReader(content));
	        String line;
	        while ((line = reader.readLine()) != null) {
	          builder.append(line);
	        }
	      } else {
	        Log.e(TAG, "Failed to download file");
	      }
	    } catch (ClientProtocolException e) {
	      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	    return builder.toString();
	  }

}
