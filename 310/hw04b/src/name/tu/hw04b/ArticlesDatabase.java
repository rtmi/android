package name.tu.hw04b;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ArticlesDatabase {
	String TAG = "ArticlesDatabase";

	public static final String TABLE_ARTICLES = "articles";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_TITLE = "title";
	public static final String COLUMN_DATE = "date";
	public static final String COLUMN_ICON = "icon";
	public static final String COLUMN_CONTENT = "content";
	
	private static final String DATABASE_NAME = "articles.db";
	private static final int DATABASE_VERSION = 1;
	private ArticlesOpenHelper mhelper;

	public ArticlesDatabase(Context context) {
		mhelper = new ArticlesOpenHelper(context);
	}

	public SQLiteDatabase getReadableDatabase(){
		return mhelper.getReadableDatabase();
	}

    private static class ArticlesOpenHelper extends SQLiteOpenHelper {
    	String TAG = "ArticlesOpenHelper";
        private final Context mHelperContext;
        private SQLiteDatabase mDatabase;
    	private static final String ARTICLES_CREATE = "create table "
    			  + TABLE_ARTICLES + "(" + COLUMN_ID
    			  + " integer primary key autoincrement, " + COLUMN_TITLE
    			  + " text , " + COLUMN_DATE
    			  + " text , " + COLUMN_ICON
    			  + " text , " + COLUMN_CONTENT
    			  + " text not null);";
    	
    	public ArticlesOpenHelper(Context context) {
	        super(context, DATABASE_NAME, null, DATABASE_VERSION);
	        Log.i(TAG, "Database ctor");
	        mHelperContext = context;
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
	        Log.i(TAG, "Database create");
            mDatabase = db;
			db.execSQL(ARTICLES_CREATE);
			loadArchive();
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	        Log.i(TAG, "Database upgrade");
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTICLES);
		    onCreate(db);
		}
		
        public long addArticle(String title, String content) {
	        Log.i(TAG, "Database insert");
            ContentValues initialValues = new ContentValues();
            initialValues.put(COLUMN_TITLE, title);
            initialValues.put(COLUMN_CONTENT, content);

            return mDatabase.insert(TABLE_ARTICLES, null, initialValues);
        }
		
	    private void loadArchive() {
	        Log.i(TAG, "Database load thread");
	        new Thread(new Runnable() {
	            public void run() {
	                try {
	                    loadArticles();
	                } catch (IOException e) {
	                    throw new RuntimeException(e);
	                }
	            }
	        }).start();
	    }

	    private void loadArticles() throws IOException {
	        Log.i(TAG, "Database thread started");
	        final Resources resources = mHelperContext.getResources();
	        InputStream inputStream = resources.openRawResource(R.raw.articles);
	        //BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	        

	        try {
	        	Document xml = parseXML(inputStream);
	        	NodeList ls = xml.getElementsByTagName("item");
	        	int max = ls.getLength();
	        	for(int i=0; i < max; i++){
	        		Element n = (Element)ls.item(i);
	        		if (n==null || !n.hasChildNodes()){continue;}
	        		NodeList titlels = n.getElementsByTagName("title");
	        		NodeList contentls = n.getElementsByTagName("content");
	        		String title = titlels.item(0).getTextContent();
	        		String content = contentls.item(0).getTextContent();
	        		long id = addArticle(title, content);
	                if (id < 0) {
	                    Log.e(TAG, "unable to add article: " + title);
	                }
	        	}
	        } finally {
	            inputStream.close();
	        }
	        Log.d(TAG, "DONE loading articles.");
	    }
	    public Document parseXML(InputStream source) {
	        try {
	            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	            dbf.setNamespaceAware(false);
	            dbf.setValidating(false);
	            DocumentBuilder db = dbf.newDocumentBuilder();
	            return db.parse(source);
	        } catch (Exception e) {
	            e.printStackTrace();
	            return null;
	        }
	     }    	
    }

}
