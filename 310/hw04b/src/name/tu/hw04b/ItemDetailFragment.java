package name.tu.hw04b;

import android.database.Cursor;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import name.tu.hw04b.dummy.DummyContent;

/**
 * A fragment representing a single Item detail screen. This fragment is either
 * contained in a {@link ItemListActivity} in two-pane mode (on tablets) or a
 * {@link ItemDetailActivity} on handsets.
 */
public class ItemDetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

	String TAG = "ItemDetailFragment";
	
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private DummyContent.DummyItem mItem;
	
	private String mItemposition;
	private View mview;
	private static final int LOADER_ID = 13;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ItemDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "Detail fragment started");

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			mItemposition = getArguments().getString(ARG_ITEM_ID);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.i(TAG, "Detail fragment create view");
		View rootView = inflater.inflate(R.layout.fragment_item_detail,
				container, false);
		mview = rootView;

		getLoaderManager().initLoader(LOADER_ID, null, this);
		return rootView;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		Log.i(TAG, "Detail fragment create loader");
		String getarti = mItemposition;
		Builder uri = ArticlesProvider.CONTENT_URI.buildUpon().appendPath(getarti);
		String[] projection = { ArticlesDatabase.COLUMN_CONTENT };
	    CursorLoader cursorLoader = new CursorLoader(getActivity(),
	        uri.build(), projection, null, null, null);
		return cursorLoader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
		Log.i(TAG, "Detail fragment cursor loaded");

		switch(arg0.getId()){
		case LOADER_ID:
			int index = arg1.getColumnIndex(ArticlesDatabase.COLUMN_CONTENT);
			arg1.moveToFirst();
			String text = arg1.getString(index);
			// Show the content as text in a TextView.
			((TextView) mview.findViewById(R.id.item_detail))
			.setText(text);

			break;
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		// TODO Auto-generated method stub
		
	}
}
