package name.tu.hw04b;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

public class ArticlesProvider extends ContentProvider {
    String TAG = "ArticleProvider";

    private static final String ARTICLES_BASE_PATH = "articles";
    public static String AUTHORITY = "name.tu.hw04b.ArticlesProvider";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + ARTICLES_BASE_PATH);

    public static final String ARCHIVE_MIME_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE +
                                                  "/vnd.tu.hw04.article";
    public static final String ARTICLE_MIME_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE +
                                                       "/vnd.tu.hw04.article";

    private ArticlesDatabase mArchive;
    private static final int SEARCH_ARCHIVE = 0;
    private static final int GET_ARTICLE = 1;
    private static final UriMatcher sURIMatcher = buildUriMatcher();
    private static UriMatcher buildUriMatcher() {
        UriMatcher matcher =  new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(AUTHORITY, ARTICLES_BASE_PATH, SEARCH_ARCHIVE);
        matcher.addURI(AUTHORITY, ARTICLES_BASE_PATH+"/#", GET_ARTICLE);

        return matcher;
    }
    
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri uri) {
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
        case SEARCH_ARCHIVE:
            return ARCHIVE_MIME_TYPE;
        case GET_ARTICLE:
            return ARTICLE_MIME_TYPE;
        default:
            return null;
        }
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean onCreate() {
		Log.i(TAG, "Provider ctor");
		mArchive = new ArticlesDatabase(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		Log.i(TAG, "Provider being queried");
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(ArticlesDatabase.TABLE_ARTICLES);
		switch(sURIMatcher.match(uri)){
		case SEARCH_ARCHIVE:
			break;
		case GET_ARTICLE:
			queryBuilder.appendWhere(ArticlesDatabase.COLUMN_ID + "="
                    + uri.getLastPathSegment());
			break;
        default:
            throw new IllegalArgumentException("Unknown Uri: " + uri);
		}

		/* Send the database the sql select and return the resultset in the cursor. */
        Cursor cursor = queryBuilder.query(mArchive.getReadableDatabase(),
                projection, selection, selectionArgs, null, null, sortOrder);

        /* Let the subscribers know the cursor state changed. */
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
