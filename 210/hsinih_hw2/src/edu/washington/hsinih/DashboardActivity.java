package edu.washington.hsinih;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class DashboardActivity extends Activity {
	public static final String FILENAME = "hsinih_hw2";
	public static final String TOREAD = "toread";
	public static final String TITLE = "title";
	public static String TAG;
	private String CLASS;

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = getString(R.string.app_name);
        CLASS = getComponentName().getShortClassName();
        setContentView(R.layout.main);
    	Log.d(TAG, String.format(getString(R.string.act_Created), CLASS));
    }
    @Override
    public void onPause() {
    	super.onPause();
    	Log.d(TAG, String.format(getString(R.string.act_Paused), CLASS));
    }
    @Override
    public void onResume() {
    	super.onResume();
    	Log.d(TAG, String.format(getString(R.string.act_Resumed), CLASS));
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	Log.d(TAG, String.format(getString(R.string.act_Transient), CLASS));
    }
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
    	super.onRestoreInstanceState(savedInstanceState);
    	Log.d(TAG, String.format(getString(R.string.act_TransientRestored), CLASS));
    }
    public void buttonClickHandler(View target) {
    	switch(target.getId()) {
    	case R.id.button1:
        	Log.d(TAG, String.format(getString(R.string.btnSearch_Clicked), CLASS));
    		startActivity(new Intent(Intent.ACTION_GET_CONTENT));
    		break;
    	case R.id.button2:
        	Log.d(TAG, String.format(getString(R.string.btnAdd_Clicked), CLASS));
    		startActivity(new Intent(Intent.ACTION_INSERT_OR_EDIT));
    		break;
    	}
    }
}