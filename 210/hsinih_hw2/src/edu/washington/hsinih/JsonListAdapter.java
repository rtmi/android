package edu.washington.hsinih;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

public class JsonListAdapter extends BaseAdapter implements ListAdapter {

	private JSONObject _items;
	private Context _context;
	private int _layoutResourceId;
	private int _textViewResourceId;
	private int _ratingResourceId;

	public JsonListAdapter(Context context, int layoutResourceId, int textViewResourceId, int ratingResourceId, String json) {
		try
		{
			_context = context;
			_layoutResourceId = layoutResourceId;
			_textViewResourceId = textViewResourceId;
			_ratingResourceId = ratingResourceId;
			_items = new JSONObject(json);
		}
		catch (org.json.JSONException e)
		{
			Log.e(AddActivity.TAG, "Invalid JSON argument.", e);
			e.printStackTrace();
		}
	}

	public int getCount() {
		try
		{
			return _items.getJSONArray(AddActivity.TOREAD).length();
		}
		catch (org.json.JSONException e)
		{
			Log.e(AddActivity.TAG, "Failed to count JSON array.", e);
			e.printStackTrace();
		}

		return 0;
	}

	public Object getItem(int position) {
		try
		{
			return _items.getJSONArray(AddActivity.TOREAD).getJSONObject(position);
		}
		catch (org.json.JSONException e)
		{
			Log.e(AddActivity.TAG, "Failed to index JSON array.", e);
			e.printStackTrace();
		}

		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		TextView label;
		View view = convertView;
		RatingBar rating;
		if (view == null)
		{
			LayoutInflater vi = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = vi.inflate(_layoutResourceId, null);
		}
		try
		{
			rating = (RatingBar) view.findViewById(_ratingResourceId);
			label = (TextView) view.findViewById(_textViewResourceId);
			JSONObject book = _items.getJSONArray(AddActivity.TOREAD).getJSONObject(position);
			label.setText(book.getString(AddActivity.TITLE));
			rating.setRating((float)book.getDouble(AddActivity.RATING));
		}
		catch (org.json.JSONException e)
		{
			Log.e(AddActivity.TAG, "Failed to get JSON fields.", e);
			e.printStackTrace();
		}

		return view;
	}
}