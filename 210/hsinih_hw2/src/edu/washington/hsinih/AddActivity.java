package edu.washington.hsinih;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class AddActivity extends ListActivity {
	public static final String TAG = "hsinih_hw2";
	public static final String FILENAME = "hsinih_hw2";
	public static final String TOREAD = "toread";
	public static final String TITLE = "title";
	public static final String RATING = "rating";
	public static final String PREDATA = "{\"toread\":[" +
			"{\"title\":\"To be or not to be, that is the question\",\"rating\":5}," +
			"{\"title\":\"If you can dodge a wrench, you can dodge a ball.\",\"rating\":1}," +
			"{\"title\":\"We have met the enemy and he is us.\",\"rating\":4}," +
			"{\"title\":\"Beauty is in the eye of the beholder.\",\"rating\":3}," +
			"{\"title\":\"It\'s only a flesh wound.\",\"rating\":5}," +
			"{\"title\":\"Never go in against a Sicilian when death is on the line!\",\"rating\":5}," +
			"{\"title\":\"Hokey religions and ancient weapons are no match for a good blaster at your side, kid.\",\"rating\":4}," +
			"{\"title\":\"Cogito, ergo sum.\",\"rating\":3}," +
			"{\"title\":\"whenever mankind interferes with a less developed civilization, the results are invariably disastrous.\",\"rating\":3}," +
			"{\"title\":\"In order to keep ourselves safe, we must first take the safety off.\",\"rating\":0}" +
			"]}";
	public static final int RATING_DIALOG_ID = 0;
	private String CLASS;
	private JSONObject _catalog;
	private int _selecteditempos;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add);
		initializeCachedVariables();
		Log.v(TAG, String.format(getString(R.string.act_Created), CLASS));

		redrawListView();

		ListView lv = getListView();
		lv.setTextFilterEnabled(true);

		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				//Toast.makeText(getApplicationContext(), ((TextView) view.findViewById(R.id.textView1)).getText(), Toast.LENGTH_SHORT).show();
				_selecteditempos = position;
				showDialog(RATING_DIALOG_ID);
			}
		});
	}


	@Override
	protected void onResume() {
		super.onResume();
		Log.v(TAG, String.format(getString(R.string.act_Resumed), CLASS));
	}

	@Override
	protected void onPause() {
		super.onPause();
    	Log.v(TAG, String.format(getString(R.string.act_Paused), CLASS));
    	if (_catalog != null)
    	{
    		// TODO only update when dirty

    		SharedPreferences settings = getSharedPreferences(FILENAME, MODE_WORLD_WRITEABLE);
    		SharedPreferences.Editor editor = settings.edit();
    		editor.putString(TOREAD, _catalog.toString());
    		editor.commit();
        	Log.v(TAG, _catalog.toString());
        	saveRatings();
    	}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
    	Log.v(TAG, String.format(getString(R.string.act_Transient), CLASS));
		if (_catalog != null)
		{
			outState.putString(TOREAD, _catalog.toString());
		}
	}

	@Override
	protected void onRestoreInstanceState(Bundle state) {
		super.onRestoreInstanceState(state);
    	Log.v(TAG, String.format(getString(R.string.act_TransientRestored), CLASS));
    	if (state.containsKey(TOREAD))
    	{
    		try
    		{
    			_catalog = new JSONObject(state.getString(TOREAD));
    		}
    		catch (org.json.JSONException e)
    		{
    	    	Log.e(TAG, e.toString());
    			e.printStackTrace();
    		}
    	}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog;
		switch(id)
		{
			case RATING_DIALOG_ID:
				Context mContext = getApplicationContext();
				LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
				final View layout = inflater.inflate(R.layout.rating_dialog, (ViewGroup)findViewById(R.id.layout_ratingdialog));

				AlertDialog.Builder _builder = new AlertDialog.Builder(this);
				_builder.setView(layout)
					.setPositiveButton("Save", 
							new DialogInterface.OnClickListener() {
								
								public void onClick(DialogInterface dialog, int which) {
									RatingBar bar = (RatingBar)layout.findViewById(R.id.ratingDialogBar);
									float rval = bar.getRating();
									String val = Float.toString(rval);
									try
									{
										JSONArray list = _catalog.getJSONArray(TOREAD);
										JSONObject record = list.getJSONObject(_selecteditempos);
										record.put(RATING, rval);
										list.put(_selecteditempos, record);
										_catalog.put(TOREAD, list);
									}
									catch (org.json.JSONException e)
									{
										Log.e(TAG, "Failed to access cached variable.", e);
										e.printStackTrace();
									}

									Toast.makeText(getApplicationContext(), 
											String.format("You rated quote# %d at %s", _selecteditempos, val), 
											Toast.LENGTH_SHORT).show();
								}
							});
				dialog = _builder.create();
				break;
			default:
				dialog = null;
		}
		return dialog;
	}

	public void btnClick(View target) throws org.json.JSONException {

		String val = ((EditText)findViewById(R.id.editText1)).getText().toString();

		if (val.length() == 0)
		{
			return;
		}

		JSONArray titles;
		if (_catalog != null)
		{
			titles = _catalog.getJSONArray(TOREAD);
		}
		else
		{
			_catalog = new JSONObject();
			titles = new JSONArray();
			_catalog.put(TOREAD, titles);
		}

		JSONObject single = new JSONObject();
		single.put(TITLE, val);
		single.put(RATING, 0); // TODO
		titles.put(single);

		redrawListView();
	}

	private void initializeCachedVariables() {

        CLASS = getComponentName().getShortClassName();

    	if (_catalog == null)
    	{
	    	SharedPreferences settings = getSharedPreferences(FILENAME, MODE_WORLD_WRITEABLE);

	    	String json = settings.getString(TOREAD, "");

    		try
    		{
		    	if (json != "")
		    	{
	    			_catalog = new JSONObject(json);
		    	}
		    	else
		    	{
		    		// Pre-populate since the prefs don't exist.
		    		_catalog = new JSONObject(PREDATA);
		    	}
    		}
    		catch (org.json.JSONException e)
    		{
    			Log.e(TAG, "Failed to initialize cached JSON.", e);
    			e.printStackTrace();
    		}
    	}
	}

	private void redrawListView() {
		this.setListAdapter(new JsonListAdapter(this, R.layout.additem, R.id.textView1, R.id.ratingBarMini,
				_catalog.toString()));
	}

	private void saveRatings() {
		ListView lv = getListView();
		int max = lv.getCount();
		Log.v(TAG, String.format("row totals %d", max));
		for (int row=0; row< max; row++)
		{
			View li = lv.getChildAt(row);
			//RatingBar rating = (RatingBar) li.findViewById(R.id.ratingBar1);

			// TODO save values

			//Log.v(TAG, String.format("row %d, stars: %f", row, rating.getRating()));
		}
	}
}

