/**
 * Android210 Fall 2011
 * Assignment.3 Nov 8, 2011
 * by Hsinih Tu
 * 
 * 
 */
package edu.washington.hsinih;

import org.json.JSONObject;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class BrowseActivity extends ListActivity {

	protected Dialog mSplashDialog;
	public static final String TAG = "hsinih_hw3";
	public static final String FILENAME = "hsinih_hw3";
	public static final String SELECTED = "selecteditempos";
	public static final String TOREAD = "toread";
	public static final String TITLE = "title";
	public static final String ALLOWED = "allowed";
	public static final String TYPE = "type";
	public static final String SIZE = "size";
	public static final String PREDATA = "{\"toread\":[" +
			"{\"size\":12,\"title\":\"W3\",\"allowed\":\"Tent\",\"type\":\"Primitive Walk-in\"}," +
			"{\"size\":12,\"title\":\"W6\",\"allowed\":\"Tent\",\"type\":\"Primitive Walk-in\"}," +
			"{\"size\":12,\"title\":\"W9\",\"allowed\":\"Tent\",\"type\":\"Primitive Walk-in\"}," +
			"{\"size\":18,\"title\":\"S2\",\"allowed\":\"Van/Camper\",\"type\":\"Standard\"}," +
			"{\"size\":18,\"title\":\"S5\",\"allowed\":\"Van/Camper\",\"type\":\"Standard\"}," +
			"{\"size\":18,\"title\":\"S8\",\"allowed\":\"Van/Camper\",\"type\":\"Standard\"}," +
			"{\"size\":32,\"title\":\"T1\",\"allowed\":\"Lg Trailer/Motorhome 18-32ft\",\"type\":\"Electrical Water Sewer Hook-up\"}," +
			"{\"size\":32,\"title\":\"T4\",\"allowed\":\"Lg Trailer/Motorhome 18-32ft\",\"type\":\"Electrical Water Sewer Hook-up\"}," +
			"{\"size\":32,\"title\":\"T7\",\"allowed\":\"Lg Trailer/Motorhome 18-32ft\",\"type\":\"Electrical Water Sewer Hook-up\"}," +
			"{\"size\":32,\"title\":\"T10\",\"allowed\":\"Lg Trailer/Motorhome 18-32ft\",\"type\":\"Electrical Water Sewer Hook-up\"}" +
			"]}";
	private String CLASS;
	private JSONObject _catalog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MyStateSaver data = (MyStateSaver) getLastNonConfigurationInstance();
        if (data != null) {
            // Show splash screen if still loading
            if (data.showSplashScreen) {
                showSplashScreen();
            }
            setContentView(R.layout.campsites);
     
            // Rebuild your UI with your saved state here
    		redrawListView();
        } else {
            showSplashScreen();
            setContentView(R.layout.campsites);
            // Do your heavy loading here on a background thread
    		initializeCachedVariables();
    		redrawListView();

    		ListView lv = getListView();
    		lv.setTextFilterEnabled(true);

    		lv.setOnItemClickListener(new OnItemClickListener() {
    			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    				Intent i = new Intent(getApplicationContext(), DetailActivity.class);
    				i.putExtra(SELECTED, position);
    				startActivity(i);
    			}
    		});

        }

        Log.v(TAG, String.format(getString(R.string.act_Created), CLASS));
    }

	@Override
	protected void onPause() {
		super.onPause();
    	Log.v(TAG, String.format(getString(R.string.act_Paused), CLASS));
    	if (_catalog != null)
    	{
    		// TODO only update when dirty

    		SharedPreferences settings = getSharedPreferences(FILENAME, MODE_WORLD_WRITEABLE);
    		SharedPreferences.Editor editor = settings.edit();
    		editor.putString(TOREAD, _catalog.toString());
    		editor.commit();
        	Log.v(TAG, _catalog.toString());
    	}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
    	Log.v(TAG, String.format(getString(R.string.act_Transient), CLASS));
		if (_catalog != null)
		{
			outState.putString(TOREAD, _catalog.toString());
		}
	}

	@Override
	protected void onRestoreInstanceState(Bundle state) {
		super.onRestoreInstanceState(state);
    	Log.v(TAG, String.format(getString(R.string.act_TransientRestored), CLASS));
    	if (state.containsKey(TOREAD))
    	{
    		try
    		{
    			_catalog = new JSONObject(state.getString(TOREAD));
    		}
    		catch (org.json.JSONException e)
    		{
    	    	Log.e(TAG, e.toString());
    			e.printStackTrace();
    		}
    	}
	}

    @Override
    public Object onRetainNonConfigurationInstance() {
        MyStateSaver data = new MyStateSaver();
        // Save your important data here
     
        if (mSplashDialog != null) {
            data.showSplashScreen = true;
            removeSplashScreen();
        }
        return data;
    }

    /**
     * Removes the Dialog that displays the splash screen
     */
    protected void removeSplashScreen() {
        if (mSplashDialog != null) {
            mSplashDialog.dismiss();
            mSplashDialog = null;
        }
    }

    /**
     * Shows the splash screen over the full Activity
     */
    protected void showSplashScreen() {
        mSplashDialog = new Dialog(this, R.style.SplashScreen);
        mSplashDialog.setContentView(R.layout.splashscreen);
        mSplashDialog.setCancelable(false);
        mSplashDialog.show();
     
        // Set Runnable to remove splash screen just in case
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
          public void run() {
            removeSplashScreen();
          }
        }, 3000);
    }

    /**
     * Simple class for storing important data across config changes
     */
    private class MyStateSaver {
        public boolean showSplashScreen = false;
        // Your other important fields here
    }


	private void initializeCachedVariables() {

        CLASS = getComponentName().getShortClassName();

    	if (_catalog == null)
    	{
	    	SharedPreferences settings = getSharedPreferences(FILENAME, MODE_WORLD_WRITEABLE);

	    	String json = settings.getString(TOREAD, "");

    		try
    		{
		    	if (json != "")
		    	{
	    			_catalog = new JSONObject(json);
		    	}
		    	else
		    	{
		    		// Pre-populate since the prefs don't exist.
		    		_catalog = new JSONObject(PREDATA);
		    	}
    		}
    		catch (org.json.JSONException e)
    		{
    			Log.e(TAG, "Failed to initialize cached JSON.", e);
    			e.printStackTrace();
    		}
    	}
	}

	private void redrawListView() {
		this.setListAdapter(new JsonListAdapter(this, R.layout.campsite, R.id.campName, R.id.campThumbnail,
				_catalog.toString()));
	}

}

/*
* Sources:
* http://blog.iangclifton.com/2011/01/01/android-splash-screens-done-right/
*
*/