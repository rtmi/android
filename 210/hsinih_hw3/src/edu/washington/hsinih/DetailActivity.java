package edu.washington.hsinih;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailActivity extends Activity {

	private JSONObject _catalog;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.campsitedetail);
		
		// TODO Pull selected index from intent state info, then adjust source image
		// attribute in detail layout.
		Bundle extras = getIntent().getExtras();
		if (extras == null)
		{
			return;
		}

		int selecteditempos = extras.getInt(BrowseActivity.SELECTED);

		InitializeCatalog();
		SetCampsite(selecteditempos);

	}

	private void InitializeCatalog()
	{
    	SharedPreferences settings = getSharedPreferences(BrowseActivity.FILENAME, MODE_WORLD_WRITEABLE);

    	String json = settings.getString(BrowseActivity.TOREAD, "");

		try
		{
			_catalog = new JSONObject(json);
		}
		catch (org.json.JSONException e)
		{
			Log.e(BrowseActivity.TAG, "Failed to retrieve JSON from persistent storage.", e);
			e.printStackTrace();
		}

	}

	private void SetCampsite(int index)
	{
		try
		{
			JSONArray ls = _catalog.getJSONArray(BrowseActivity.TOREAD);
			JSONObject item = ls.getJSONObject(index);
			String title = item.getString(BrowseActivity.TITLE);

			int size = item.getInt(BrowseActivity.SIZE);
			String allowed = item.getString(BrowseActivity.ALLOWED);
			String type = item.getString(BrowseActivity.TYPE);
			MapCampsite(title, size, allowed, type);

			Toast.makeText(getApplicationContext(), String.format("Site %s", title), 3600).show();
		}
		catch (org.json.JSONException e)
		{
			Log.e(BrowseActivity.TAG, "Failed to extract JSON fields.", e);
			e.printStackTrace();
		}
	}

	private void MapCampsite(String title, int size, String allowed, String type)
	{
		((TextView)findViewById(R.id.campDescr)).setText(String.format("Type: %s", type));
		((TextView)findViewById(R.id.campAllowed)).setText(String.format("Allowed: %s", allowed));
		((TextView)findViewById(R.id.campSize)).setText(String.format("Size: %d", size));

		ImageView photo = (ImageView)findViewById(R.id.campPhoto);
		int resourceId = getResources().getIdentifier("edu.washington.hsinih:drawable/"+title.toLowerCase(), null, null);
		photo.setImageResource(resourceId);
	}
}
