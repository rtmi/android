package edu.washington.hsinih;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

public class JsonListAdapter extends BaseAdapter implements ListAdapter {


	private JSONObject _items;
	private Context _context;
	private int _layoutResourceId;
	private int _textViewResourceId;
	private int _thumbnailResourceId;

	public JsonListAdapter(Context context, int layoutResourceId, int textViewResourceId, int thumbnailResourceId, String json) {
		try
		{
			_context = context;
			_layoutResourceId = layoutResourceId;
			_textViewResourceId = textViewResourceId;
			_thumbnailResourceId = thumbnailResourceId;
			_items = new JSONObject(json);
		}
		catch (org.json.JSONException e)
		{
			Log.e(BrowseActivity.TAG, "Invalid JSON argument.", e);
			e.printStackTrace();
		}
	}

	public int getCount() {
		try
		{
			return _items.getJSONArray(BrowseActivity.TOREAD).length();
		}
		catch (org.json.JSONException e)
		{
			Log.e(BrowseActivity.TAG, "Failed to count JSON array.", e);
			e.printStackTrace();
		}

		return 0;
	}

	public Object getItem(int position) {
		try
		{
			return _items.getJSONArray(BrowseActivity.TOREAD).getJSONObject(position);
		}
		catch (org.json.JSONException e)
		{
			Log.e(BrowseActivity.TAG, "Failed to index JSON array.", e);
			e.printStackTrace();
		}

		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		TextView label;
		View view = convertView;
		ImageView thumb;
		if (view == null)
		{
			LayoutInflater vi = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = vi.inflate(_layoutResourceId, null);
		}
		try
		{
			thumb = (ImageView) view.findViewById(_thumbnailResourceId);
			label = (TextView) view.findViewById(_textViewResourceId);
			JSONObject book = _items.getJSONArray(BrowseActivity.TOREAD).getJSONObject(position);
			label.setText(book.getString(BrowseActivity.TITLE));
			thumb.setImageResource(mapThumbnail(book.getInt("size")));
		}
		catch (org.json.JSONException e)
		{
			Log.e(BrowseActivity.TAG, "Failed to get JSON fields.", e);
			e.printStackTrace();
		}

		return view;
	}

	private int mapThumbnail(int size)
	{
		int resourceId;
		switch (size)
		{
		case 18:
			resourceId = R.drawable.campmedium;
			break;
		case 32:
			resourceId = R.drawable.camplarge;
			break;
		default:
			resourceId = R.drawable.campsmall;
			break;
		}
		return resourceId;
	}
}
