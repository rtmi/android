package edu.uw.tu.homework6;

public class FoodTruckModel {
	  private long truckid;
	  private String truck;
	  private long streetid;
	  private String street;
	  private String zip;
	  private long timeid;
	  private String day;
	  private String open;

	  public long getTruckId() {
	    return truckid;
	  }

	  public void setTruckId(long truckid) {
	    this.truckid = truckid;
	  }

	  public String getTruck() {
	    return truck;
	  }

	  public void setTruck(String truck) {
	    this.truck = truck;
	  }

	  public long getStreetId() {
	    return streetid;
	  }

	  public void setStreetId(long streetid) {
	    this.streetid = streetid;
	  }

	  public String getStreet() {
	    return street;
	  }

	  public void setStreet(String street) {
	    this.street = street;
	  }

	  public String getZip() {
	    return zip;
	  }

	  public void setZip(String zip) {
	    this.zip = zip;
	  }

	  public long getTimeId() {
	    return timeid;
	  }

	  public void setTimeId(long timeid) {
	    this.timeid = timeid;
	  }

	  public String getDay() {
	    return day;
	  }

	  public void setDay(String day) {
	    this.day = day;
	  }

	  public String getOpen() {
	    return open;
	  }

	  public void setOpen(String open) {
	    this.open = open;
	  }

	  // Will be used by the ArrayAdapter in the ListView
	  @Override
	  public String toString() {
	    return truck;
	  }
}
