package edu.uw.tu.homework6;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

@TargetApi(11)
public class EditZipDialogFragment extends DialogFragment {

	public interface EditZipDialogListener{
        public void onEditZipDialogPositiveClick(String zip);
        public void onEditZipDialogNegativeClick(DialogFragment dialog);
    }
    
	EditZipDialogListener mListener;

	private EditText mEditTextEditzip;

	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
        	mListener = (EditZipDialogListener) activity;
        }catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement EditZipDialogListener");
        }
    }

    @Override
	public Dialog onCreateDialog(Bundle SavedInstanceState){
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view =inflater.inflate(R.layout.dialog_editzip, null);
		mEditTextEditzip = (EditText) view.findViewById(R.id.editzip);

		builder.setView(view).setTitle(R.string.editzip_title)
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					mListener.onEditZipDialogPositiveClick(mEditTextEditzip.getText().toString());
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					mListener.onEditZipDialogNegativeClick(EditZipDialogFragment.this);
				}
			});
	
		return builder.create();
	}
}
