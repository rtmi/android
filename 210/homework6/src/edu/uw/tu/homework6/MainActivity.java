package edu.uw.tu.homework6;

import java.util.List;

import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.ListActivity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;

@TargetApi(11)
public class MainActivity extends ListActivity
implements AddTruckDialogFragment.AddTruckDialogListener,
AddWeekdayDialogFragment.AddWeekdayDialogListener, EditZipDialogFragment.EditZipDialogListener {

	private FoodTruckDataSource datasource;
	private FoodTruckModel foodtruck;
	private SharedPreferences pref;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		pref = this.getSharedPreferences("edu.uw.tu.homework6",MODE_WORLD_WRITEABLE);
		
		datasource = new FoodTruckDataSource(this);
		datasource.open();

		DisplayTruckList();
	}

    public void onClick(View view) {
    	new AddTruckDialogFragment().show(getFragmentManager(), "addtruck");
    }

    public void onPrefClick(View view) {
    	new EditZipDialogFragment().show(getFragmentManager(), "preferences");
    }

    @Override
    protected void onResume() {
      datasource.open();
      super.onResume();
    }

    @Override
    protected void onPause() {
      datasource.close();
      super.onPause();
    }

	public void onDialogPositiveClick(String truck, String street, String zip) {
        foodtruck = datasource.createModel(truck, street, zip);

        new AddWeekdayDialogFragment().show(getFragmentManager(), "addweekdays");

        @SuppressWarnings("unchecked")
        ArrayAdapter<FoodTruckModel> adapter = (ArrayAdapter<FoodTruckModel>) getListAdapter();
        adapter.add(foodtruck);
        adapter.notifyDataSetChanged();
	}

	public void onDialogNegativeClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}

	public void onAddWeekdayDialogPositiveClick(List<EventTimeModel> schedule) {
		datasource.createWeeklyEvent(foodtruck, schedule);
	}

	public void onAddWeekdayDialogNegativeClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}

	public void onEditZipDialogPositiveClick(String zip) {
		Editor ed = pref.edit();
		ed.putString("edu.uw.tu.homework6", zip);
		ed.commit();
		DisplayTruckList();
	}

	public void onEditZipDialogNegativeClick(DialogFragment dialog) {
		// TODO Auto-generated method stub
		
	}

	private void DisplayTruckList(){
		List<FoodTruckModel> values;
        String zip = pref.getString("edu.uw.tu.homework6", null);

        if (zip == null){
			values = datasource.getAll();
        }else{
        	values = datasource.getTrucksByZip(zip);
        }

        ArrayAdapter<FoodTruckModel> adapter = new ArrayAdapter<FoodTruckModel>(this, android.R.layout.simple_list_item_1, values);
		setListAdapter(adapter);
	}
}
